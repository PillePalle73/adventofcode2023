# Advent of Code 2023, Day 2a
file1 = open('input.txt ', 'r')
lines = file1.readlines()

resultsum = 0

# 12 red cubes, 13 green cubes, and 14 blue cubes

sum = 0
for line in lines:
    print(line)
    left, right = line.split(":")
    
    game = int(left.split(" ")[1])
    
    draws = right.split(";")
    ok=True
    for draw in draws:
        print( draw )
        combis = draw.split(",")
        for combi in combis:
            print( combi.split() )
            v,c = combi.split()
            if c=="red" and int(v)>12:
                ok=False
            if c=="green" and int(v)>13:
                ok=False
            if c=="blue" and int(v)>14:
                ok=False
    if ok:
        sum = sum + game
        
print(sum)

    