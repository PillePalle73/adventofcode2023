# Advent of Code 2023, Day 2b
file1 = open('input.txt ', 'r')
lines = file1.readlines()

resultsum = 0

# 12 red cubes, 13 green cubes, and 14 blue cubes

sum = 0
for line in lines:
    print(line)
    left, right = line.split(":")
    
    game = int(left.split(" ")[1])
    
    draws = right.split(";")
    ok=True
    maxred = 0
    maxblue = 0
    maxgreen = 0
    for draw in draws:
        print( draw )
        combis = draw.split(",")
        for combi in combis:
            print( combi.split() )
            v,c = combi.split()
            if c=="red":
                if int(v)>maxred:
                    maxred = int(v)
            if c=="green":
                if int(v)>maxgreen:
                    maxgreen = int(v)
            if c=="blue":
                if int(v)>maxblue:
                    maxblue = int(v)
    power = maxred*maxblue*maxgreen
    print(power)
    sum = sum + power
        
print(sum)

    