# Advent of Code 2023, Day 4a
import re
import string 

file1 = open('input.txt ', 'r')
lines = file1.readlines()

#Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
#Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
#Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
#Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
#Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
#Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11

sum = 0
for line in lines:
    print(line)
    left, right = line.strip().split(":")
    
    winners, havers = right.split('|')
    
    print(winners.strip())
    print(havers.strip())
    
    winners = re.split('\s', winners)
    havers = re.split('\s', havers)
    
    score = int(0)
    for hav in havers:
        for win in winners:
            #hav = str(hav).replace(" ","")
            hav = re.sub(r"\s+", "", str(hav)) 
            #hav = hav.translate({ord(c): None for c in string.whitespace})
            win = win.translate({ord(c): None for c in string.whitespace})
            if len(str(hav))>0 and len(str(win))>0:
                if hav == win:
                    print("***<",hav,"> vs <", win, ">***")
                    score = score + 1
    if score>1:
        score=  2 ** (score-1);
    print(score)
    sum = sum + score
print(sum)

    