# Advent of Code 2023, Day 1a
file1 = open('advent-of-code-input-1.txt ', 'r')
lines = file1.readlines()

resultsum = 0

for line in lines:
    numbers=[]
    for i, c in enumerate(line):
        if c.isdigit():
            numbers.append(c)
    addthis=10*int(numbers[0])+int(numbers[-1])
    resultsum = resultsum + int(addthis)
     
print('resultsum=', resultsum)