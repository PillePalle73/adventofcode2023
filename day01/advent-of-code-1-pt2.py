# Advent of Code 2023, Day 1b
import re
   
file1 = open('advent-of-code-input-1.txt', 'r')
lines = file1.readlines()

resultsum = 0

for line in lines:

    print(line, end='')
    
    line = line.replace("zero", "z0o")
    line = line.replace("one", "o1e") 
    line = line.replace("two", "t2o")  

    line = line.replace("three", "th3ee")   
    line = line.replace("four", "fo4r")    
    line = line.replace("five", "f5e")
    line = line.replace("six", "s6x")
    line = line.replace("seven", "s7en")
    line = line.replace("eight", "e8ht")
    line = line.replace("nine", "n9ne")

    print("->", line)
  
    numbers=[]
    for i, c in enumerate(line):    
        if c.isdigit():
                numbers.append(c)
    print(numbers)
    addthis=0
    if len(numbers)>0:
        addthis=10*int(numbers[0])+int(numbers[-1])
    print(addthis)
    resultsum = resultsum + int(addthis)    
    print("- - - - - - - - - - -")
     
print('resultsum=', resultsum)